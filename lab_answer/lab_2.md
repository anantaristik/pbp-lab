# Lab Answer
1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

## Perbedaan JSON dan XML

| XML                                                           | JSON                                                                                 |
|---------------------------------------------------------------|--------------------------------------------------------------------------------------|
| XML adalah bahasa markup dan bukan bahasa pemrograman, untuk mendefinisikan elemen          | JSON hanyalah format yang ditulis dalam JavaScript                                   |                                                                                      |
| XML disimpan sebagai tree structure                           | Data disimpan seperti map dengan pasangan key value                                  |
| Mendukung namespaces, komentar dan metadata                   | Tidak memiliki ketentuan untuk namespace, menambahkan komentar atau menulis metadata |
| Ukuran dokumen besar, sehingga struktur tag sulit dibaca      | Mudah dibaca dan ringkas, tidak ada tag atau data yang tidak terpakai                |
| XML mendukung pengkodean  UTF-8 dan UTF-16                    | JSON mendkung penyandiaksaraan UTD serta ASCII                                       |


## Perbedaan HTML dan XML
| XML                                       | HTML                                               |
|-------------------------------------------|----------------------------------------------------|
| Singkatan dari eXtensible Markup Language | Singkatan dari Hypertext Markup Language           |
| XML berfokus pada transfer data           | HTML berfokus pada penyajian data                  |
| XML Case Sensitive                        | HTML Case Insensitive                              |
| Tag pada XML dapat dikembangkan           | HTML memiliki tag terbatas                         |
| Tag pada XML tidak ditentukan sebelumnya  | HTML memiliki tag yang telah ditentukan sebelumnya |


