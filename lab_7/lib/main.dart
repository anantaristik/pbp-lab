import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';

class companyReview extends StatefulWidget {
  @override
  _companyReview createState() => _companyReview();
}

class _companyReview extends State<companyReview> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        foregroundColor: Color(0xFFDA5144),
        title: Text('COMPANY REVIEW',
            style: TextStyle(fontFamily: 'Sansita One')),
        centerTitle: true,
      ),
        body: 
        Column(
          children: <Widget>[
            Expanded(
        child: ListView.builder(
      itemBuilder: (context, index) {
        return InkWell(
          child: Padding(
            padding: EdgeInsets.only(
                top: 32.0, bottom: 32.0, left: 16.0, right: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                
                  Row(children: <Widget>[
                    Padding(padding:EdgeInsets.only(bottom:5.0, right:5.0), child:
                      Image(
                        image: AssetImage(
                            'assets/images/Png-Item-1780031.png'),
                        width: 35,
                        height: 35,
                      ),
                    ),        
                Text(
                  'pekerjaan',
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(205,73,57,50)
                  ),
                ),
                ]),
                Text(
                  'perusahaan',
                  style: TextStyle(
                    color: Colors.grey.shade600,
                  ),
                ),
                Text(
                  'WFH/WFO',
                  style: TextStyle(
                    color: Colors.grey.shade600,
                  ),
                ),
                Text(
                  'deskripsi',
                  style: TextStyle(
                    color: Colors.grey.shade600,
                  ),
                ),
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ReviewPost()),
            );
          },
        );
      },
    )
            )
  ]
)
    );
    
  }
}

class ReviewPost extends StatefulWidget {
  @override
  _ReviewPostState createState() {
    return _ReviewPostState();
  }
}

class _ReviewPostState extends State<ReviewPost>  {

  @override
  
  Widget build(BuildContext context) {
    String namaJob = "";
    String namaComp = "";
    String desc = "";
    
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        foregroundColor: Color(0xFFDA5144),
        title: Text('COMPANY REVIEW',
            style: TextStyle(fontFamily: 'Sansita One')),
        centerTitle: true,
      ),
    body:   
    Container (child: 
      ListView.builder(
        reverse: false,
        itemBuilder: (context, index) {
          return ListBody(children: <Widget>[
            Card(
              child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
              Row(
                children: <Widget>[
                  Column(children: <Widget>[
                  ]),
                  Text('    '),
                  Flexible( 
                    child: Text(
               'penulis',
                style: TextStyle(
                fontSize: 15,
                color: Color.fromRGBO(0, 0, 0, 50),
                fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          Column(children: [
            Row(
                children: <Widget>[
            ],
          ),
          ]),
          const SizedBox(height: 5),
          Text(
            'deskripsi',
              style: TextStyle(
                fontSize: 15,
                color:Color.fromRGBO(0, 0, 0, 50),
              ),
            ),
                  ],
                ),
              ),
          )]);
        }),
    ),
    floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(218,81,68,50),
        foregroundColor: Color(0xFFFFFFFF),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return addReviewForm();
          }));
        },
      ),
    );
  } 
}

class addReviewForm extends StatefulWidget {
  addReviewForm();

  @override
  _addReviewFormState createState() {
  return _addReviewFormState();
  } 
}

class _addReviewFormState extends State<addReviewForm> {
  TextEditingController message_controller = new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  int _ratings= 0;
  
  late String username;
  String? desc;
  late int id_job;
  double? valueRate;
  var now = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        foregroundColor: Color(0xFFDA5144),
        title: Text('COMPANY REVIEW',
            style: TextStyle(fontFamily: 'Sansita One')),
        centerTitle: true,
      ),
    body: Form(
      key: _formKey,
      child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
      children: [
        const Text("Give your feedback..", style: 
        TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        Padding(padding: const EdgeInsets.all(8.0),
          child: Rating((rating) {
                setState(() {
                  _ratings = rating;
                  print(_ratings);
                });
              }, 5),  
        ),
        Padding (
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
                    controller: message_controller,
                    decoration: new InputDecoration(
                      contentPadding: const EdgeInsets.symmetric(vertical: 40.0),
                      hintText: "   Give your feedback...",
                      filled: true,
                      labelText: "  Desc",
                      icon: Icon(Icons.sticky_note_2_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      print(value);
                      if (value==null || value.isEmpty) {
                        return 'Give your feedback...';
                      } else {
                        desc = value;
                      }
                      return null;
                    },
                    maxLines: 5,
                  ),
                ),
                Row (mainAxisAlignment: MainAxisAlignment.end, 
                children: <Widget>[
                  Container(
                  width: 127,
                  height: 40,
                  child: RaisedButton(
                    onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                    }
                    },
                    color: Color(0xFF34ae57),
                      child: Text('POST',
                          style: TextStyle(color: Colors.white))),
                  ),
              Text('  '),
              Container(width: 75, height: 40),
              Text('  '),
              ],
            ),
            ],
          ),
        ),
      ),
    ),
    );
  }
}

class Rating extends StatefulWidget {
  final int maximumRating;
  final Function(int) onRatingSelected;

  Rating(this.onRatingSelected, [this.maximumRating = 5]);

  @override
  _Rating createState() => _Rating();
}

class _Rating extends State<Rating> {
  int _currentRating = 0;

  Widget _buildRatingStar(int index) {
    if (index < _currentRating) {
      return Icon(Icons.star, color: Colors.orange, size: 50,);
    } else {
      return Icon(Icons.star_border_outlined, size: 50,);
    }
  }

  Widget _buildBody() {
    final stars = List<Widget>.generate(this.widget.maximumRating, (index) {
      return GestureDetector(
        child: _buildRatingStar(index),
        onTap: () {
          setState(() {
            _currentRating = index + 1;
          });

          this.widget.onRatingSelected(_currentRating);
        },
      );
    });

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: stars,
        ),
      ],
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }
}
