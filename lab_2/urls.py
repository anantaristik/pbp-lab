from django.urls import path
from . import views

app_name = 'lab_2'

urlpatterns = [
    path('', views.note_list, name='note_list'),
    path('xml', views.xml, name='xml'),
    path('json', views.json, name='json')
]