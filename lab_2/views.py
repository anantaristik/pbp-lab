from django.http.response import HttpResponse
from django.shortcuts import render
from .models import note
from django.core import serializers

# Create your views here.

def note_list(request):
    notes =  note.objects.all().values()
    response = {'note' : notes}
    return render(request, 'index.html', response)

def xml(request):
    data = serializers.serialize('xml', note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', note.objects.all())
    return HttpResponse(data, content_type="application/json")