from django.urls import path
from .views import friend_list, index

urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list, name='friend_list'),
]
