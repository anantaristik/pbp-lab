from django.shortcuts import render
from lab_4.models import notes

# Create your views here.
def index(request):
    note =  notes.objects.all().values()
    response = {'note' : note}
    return render(request, 'lab5_index.html', response)

def note_list(request):
    note =  notes.objects.all().values()
    response = {'note' : note}
    return render(request, 'lab4_note_list.html', response)