from django.urls import path

from lab_4.views import note_list
from .views import index, note_list

app_name = 'lab_5'

urlpatterns = [
    path('', index, name='index'),
    path('note-list', note_list, name='note_list')
]