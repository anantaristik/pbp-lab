from django.shortcuts import redirect, render
from lab_4.forms import NoteForm
from .models import notes

# Create your views here.
def note_list(request):
    note =  notes.objects.all().values()
    response = {'note' : note}
    return render(request, 'lab4_note_list.html', response)

def index(request):
    note =  notes.objects.all().values()
    response = {'note' : note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    form = NoteForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('lab_4:note_list')

    context['form'] = form
    return render(request, 'lab4_form.html', context)