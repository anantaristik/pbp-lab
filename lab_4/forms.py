from lab_4.models import notes
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = notes
        fields = ['to', 'fromnotes', 'title', 'message' ]

        input_attrs = {
            'type': 'text',
            'placeholder' : 'Input here'
        }

        to = forms.CharField(label='', required=True, max_length=30, 
        widget= forms.TextInput(attrs=input_attrs))

        fromnotes = forms.CharField(label='', required=True, max_length=30, 
        widget= forms.TextInput(attrs=input_attrs))

        title = forms.CharField(label='',required=True, max_length=30,
        widget=forms.TextInput(attrs=input_attrs))

        message = forms.CharField(label='',required=True, max_length=256,
        widget=forms.TextInput(attrs=input_attrs))