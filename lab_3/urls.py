from django.urls import path
from .views import friend_list, add_friend

app_name = 'lab_3'

urlpatterns = [
    path('', friend_list, name='friend_list'),
    path('add', add_friend, name='add_friend'),
]
