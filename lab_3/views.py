from django.shortcuts import render,redirect
from lab_3.models import Friend
from django.http.response import HttpResponseRedirect
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def friend_list(request):
    friends = Friend.objects.all().values()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
        
@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}

    form = FriendForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        return redirect('lab_3:friend_list')
    
    context['form'] = form
    return render(request, "lab3_form.html", context)