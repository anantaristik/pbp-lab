from lab_3.models import Friend
from django import forms

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name','npm','dob']
    error_messages = {
        'required' : 'Please Type'
    }


    input_attrs = {
            'type' : 'text',
            'placeholder' : 'Your Name', 
        }

    input_attrs_npm = {
        'type' : 'text',
        'placeholder' : 'Your NPM'
    }

    date_attrs = {
        'type' : 'date',
    }

    name = forms.CharField(label='', required=True, max_length=27, 
    widget= forms.TextInput(attrs=input_attrs))

    npm = forms.CharField(label='', required=True, max_length=10, 
    widget= forms.TextInput(attrs=input_attrs_npm))

    dob = forms.DateField(label='',required=True,
    widget=forms.DateInput(attrs=date_attrs))
    


