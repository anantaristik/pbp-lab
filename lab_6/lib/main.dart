import 'package:flutter/material.dart';
import 'package:lab_6/widgets/nav-drawer.dart';
import 'package:lab_6/widgets/card.dart'; 

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CaseWorqer',
      theme: ThemeData(
      primarySwatch: Colors.green,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: const NavDrawer(),
      appBar: AppBar(
        title: const Text('CaseWorqer', 
        style: TextStyle(fontFamily: 'SansitaOne', color: Color.fromARGB(255, 109, 18, 9))),
        // actions: [IconButton(
        //         onPressed: () {},
        //         icon: const Icon(Icons.search))]
      ),
      body: const Cards(), 
      // const Align(
      //   alignment: Alignment.topCenter,
      //   child: Text('Company Review',
      //   style: TextStyle(fontFamily: 'SansitaOne', color: Color.fromARGB(255, 218, 81, 68),
      //   fontSize: 40))),
      bottomNavigationBar: BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Beranda'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.search),
          title: Text('Loker'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.group),
          title: Text('Forum'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text('Akun'),
        ),
      ],
      currentIndex: 0,
      selectedItemColor: Colors.green,
      unselectedItemColor: Colors.grey,
      showUnselectedLabels: true,
  )
    );
  }
}
