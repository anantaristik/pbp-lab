	import 'package:flutter/material.dart';
  
class Cards extends StatelessWidget {
  const Cards({Key? key}) : super(key: key);


    @override
  Widget build(BuildContext context) {
  return Card(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: const <Widget>[
        ListTile(
          leading: Icon(Icons.local_convenience_store, size: 50),
          title: Text('Software Engineer'),
          subtitle: Text('Shopee'),
        ),
      ],
    ),
  );
  }
}