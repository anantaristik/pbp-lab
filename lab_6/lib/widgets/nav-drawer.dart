import 'package:flutter/material.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.search),
            title: const Text('Cari Lowongan'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.three_p),
            title: const Text('Buka Lowongan'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.lightbulb_outline),
            title: const Text('Tips Karier'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.group),
            title: const Text('Forum'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: const Icon(Icons.border_color),
            title: const Text('Company Review'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}