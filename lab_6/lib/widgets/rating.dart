import 'package:flutter/material.dart';
import '../widgets/rating.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Flutter Rating Control",
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
        home: RatingsPage(),

    );
  }
}

class RatingsPage extends StatefulWidget {
  @override
  _RatingsPage createState() => _RatingsPage();
}

class _RatingsPage extends State<RatingsPage> {
  int _rating = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("COMPANY RATE")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Text(
                "Bagaimana menurut anda\ntentang web kami secara\nkeseluruhan?",
                style: TextStyle(fontSize: 20,),
                textAlign: TextAlign.center,
              ),

              SizedBox(height: 10),

              Rating((rating) {
                setState(() {
                  _rating = rating as int;
                });
              }, 5),

              SizedBox(height: 10),

              Container(
                width: 200.0,
                height: 50.0,
                child: TextField(
                  style: TextStyle(
                    fontSize: 15.0,
                    height: 2.0,
                  ),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Describe your experience'
                        'type "-" is no comment',
                  ),
                ),
              ),

              SizedBox(height: 10),

              new TextButton(
                style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                  overlayColor: MaterialStateProperty.resolveWith<Color?>(
                        (Set<MaterialState> states) {
                      if (states.contains(MaterialState.hovered))
                        return Colors.blue.withOpacity(0.04);
                      if (states.contains(MaterialState.focused) ||
                          states.contains(MaterialState.pressed))
                        return Colors.blue.withOpacity(0.12);
                      return null; // Defer to the widget's default.
                    },
                  ),
                ),
                onPressed: () { },
                child: Text('POST')
              )
            ],
          ),
        ));
  }

  Rating(Null Function(rating) param0, int i) {}
}

class rating {
}